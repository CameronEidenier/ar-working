﻿using UnityEngine;
using System.Collections;

public class BackgroundPlanePositionChecker : MonoBehaviour {

	Vector3 localPosition = Vector3.zero;
	float localScale;
	float parentX;
	float targetX;

	void Update () {
	
//		localPosition = transform.localPosition;
//		parentX = transform.parent.localPosition.x;
//		localScale = transform.localScale.x;
//
//		localPosition.x = -parentX * localScale;
//
//		transform.localPosition = localPosition;


		localPosition = transform.localPosition;
		targetX = -transform.parent.localPosition.x * transform.localScale.x;
		if(localPosition.x != targetX) {
			localPosition.x = targetX;
			transform.localPosition = localPosition;
		}

//		if(localPosition.x != 0 || localPosition.y != 0) {
//
//			Debug.Log("Local Position INCORRECT");
//			localPosition.x = localPosition.y = 0;
//			transform.localPosition = localPosition;
//		}
	}
}
