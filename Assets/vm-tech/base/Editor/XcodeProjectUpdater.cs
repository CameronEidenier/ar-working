﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using UnityEditor.iOS.Xcode;
using System.IO;

public class MyBuildPostprocessor
{

//	[PostProcessBuild(-10)]
//	public static void OnPostprocessPList (BuildTarget buildTarget, string path)
//	{
//		if (buildTarget != BuildTarget.iPhone)
//			return;
//		
//		
//		string plistPath = path + "/Info.plist";
//		PlistDocument plist = new PlistDocument ();
//		plist.ReadFromString (File.ReadAllText (plistPath));
//		
//		PlistElementDict rootDict = plist.root;
//		
//		
//		////////////////////////
//		// Upsight
//		////////////////////////
//		if (rootDict ["NSAppTransportSecurity"] == null) {
//			Debug.Log ("Creating Kontagent URL Info.plist policy things.");
//			PlistElementDict transport = rootDict.CreateDict ("NSAppTransportSecurity");
//			transport.SetBoolean ("NSAllowsArbitraryLoads", true);
//			
//			PlistElementDict domains = transport.CreateDict ("NSExceptionDomains");
//			
//			string[] urls = new string[] {
//				"test-server.kontagent.com",
//				"api.geo.kontagent.net",
//				"code.jquery.com",
//				"mobile-api.geo.kontageent.net"
//			};
//			foreach (string url in urls) {
//				PlistElementDict domain = domains.CreateDict (url);
//				domain.SetBoolean ("NSIncludesSubdomains", true);
//				domain.SetBoolean ("NSExceptionAllowsInsecureHTTPLoads", true);
//			}
//		} else {
//			Debug.Log ("Kontagent - NSAppTransportSecurity already exists?");
//		}
//
//		rootDict.SetString("CFBundleDevelopmentRegionKey", "en_US");
//		
//		File.WriteAllText (plistPath, plist.WriteToString ());
//	}

	public static string DataBaseName = "mattel_viewmaster_iOS.bin";
	
	[PostProcessBuild]
	public static void OnPostprocessBuild (BuildTarget buildTarget, string path)
	{
		if (buildTarget != BuildTarget.iOS)
			return;

		Debug.Log("Starting XCode [General] Post Process");
			
		string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
		PBXProject proj = new PBXProject ();
		proj.ReadFromString (File.ReadAllText (projPath));
		
		string target = proj.TargetGuidByName ("Unity-iPhone");



//		// Touchcode frameworks  (just leaving these in here cause they'll be needed)
//		XcodePostProcessUtils.CopyAndReplaceDirectory ("NativeAssets/TouchDecode.framework", Path.Combine (path, "Touchcode/TouchDecode.framework"));
//		proj.AddFileToBuild (target, proj.AddFile ("Touchcode/TouchDecode.framework",
//			"Touchcode/TouchDecode.framework", PBXSourceTree.Source));

//		// Touchcode .bins
//		XcodePostProcessUtils.CopyAndReplaceFile ("Assets/Plugins/iOS/" + DataBaseName, Path.Combine (path, "Libraries/" + DataBaseName));
//		proj.AddFileToBuild (target, proj.AddFile ("Libraries/" + DataBaseName,
//			"Libraries/" + DataBaseName, PBXSourceTree.Absolute));

		proj.SetBuildProperty (target, "ENABLE_BITCODE", "No");		
		
		// Set a custom link flag
//		proj.UpdateBuildProperty (target, "OTHER_LDFLAGS", new string[]{"-lc++"}, new string[]{"-ObjC"});
		proj.AddBuildProperty (target, "OTHER_LDFLAGS", "-lc++");
		
		
		
		File.WriteAllText (projPath, proj.WriteToString ());
		
		Debug.Log ("Completed XCode [General] Post Process");
	}


//	[PostProcessBuild]
//	public static void OnPostprocessBuild (BuildTarget buildTarget, string path)
//	{
//		if (buildTarget != BuildTarget.iPhone)
//			return;
//
//		Debug.Log("Starting XCode [General] Post Process");
//
//		string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
//		PBXProject proj = new PBXProject ();
//		proj.ReadFromString (File.ReadAllText (projPath));
//
//		string target = proj.TargetGuidByName ("Unity-iPhone");
//
//
//		// Touchcode frameworks  (just leaving these in here cause they'll be needed)
//		XcodePostProcessUtils.CopyAndReplaceDirectory ("NativeAssets/TouchDecode.framework", Path.Combine (path, "Touchcode/TouchDecode.framework"));
//		proj.AddFileToBuild (target, proj.AddFile ("Touchcode/TouchDecode.framework",
//			"Touchcode/TouchDecode.framework", PBXSourceTree.Source));
//
//		// Touchcode .bins
//		proj.AddFileToBuild (target, proj.AddFile ("Libraries/" + DataBaseName,
//			"Libraries/" + DataBaseName, PBXSourceTree.Absolute));
//
//
//		////////////////////////
//		// Upsight
//		////////////////////////
//		//		CopyAndReplaceDirectory ("NativeAssets/Kontagent.framework", Path.Combine (path, "Kontagent/Kontagent.framework"));
//		//		proj.AddFileToBuild (target, proj.AddFile ("Kontagent/Kontagent.framework",
//		//		                                           "Kontagent/Kontagent.framework", PBXSourceTree.Source));
//		//
//		//		//proj.AddFrameworkToProject(target, "CoreData.framework", true); //redundant
//		//		proj.AddFrameworkToProject(target, "AdSupport.framework", true);           // added by .pyc
//		//		proj.AddFrameworkToProject(target, "SystemConfiguration.framework", true); // added by .pyc
//		//		proj.AddFrameworkToProject(target, "CoreTelephony.framework", true);       // added by .pyc
//		//		proj.AddFrameworkToProject(target, "libz.tbd", true);                      // NOT added by .pyc
//
//
//		////////////////////////
//		// Touchcode
//		////////////////////////
//		//		proj.SetBuildProperty (target, "FRAMEWORK_SEARCH_PATHS", "$(inherited)");
//		proj.AddBuildProperty (target, "FRAMEWORK_SEARCH_PATHS", "$(PROJECT_DIR)/Touchcode");
//
//		proj.SetBuildProperty (target, "ENABLE_BITCODE", "No");
//
//
//		// Set a custom link flag
//		//		proj.UpdateBuildProperty (target, "OTHER_LDFLAGS", new string[]{"-lc++"}, new string[]{"-ObjC"});
//		proj.AddBuildProperty (target, "OTHER_LDFLAGS", "-lc++");
//
//
//
//		File.WriteAllText (projPath, proj.WriteToString ());
//
//		Debug.Log ("Completed XCode [General] Post Process");
//	}
}
