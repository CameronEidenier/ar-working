﻿using UnityEngine;
using System.Collections;

using Vuforia;

public class ARAutoFocus : MonoBehaviour {

	private bool hasStarted = false;
	void Start() {
		
		VuforiaBehaviour vf = FindObjectOfType<VuforiaBehaviour>();
		vf.RegisterVuforiaStartedCallback(MyVuforiaStarted);		
	}
	
	private void MyVuforiaStarted() 	
	{
		hasStarted = true;
		CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);

		VuforiaBehaviour vf = FindObjectOfType<VuforiaBehaviour>();
		vf.UnregisterVuforiaStartedCallback(MyVuforiaStarted);		
	}

	void OnApplicationPause( bool pause )
	{
		if (pause == false) { 
			if(hasStarted == true)
				CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		}
	}
}
