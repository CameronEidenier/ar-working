﻿using UnityEngine;
using System.Collections;

//[ExecuteInEditMode]
public class ARIsEnabled : MonoBehaviour {

	[SerializeField] private bool IsVuforiaEnabled = false;

	protected static ARIsEnabled _instance = null;
	public static ARIsEnabled Instance { get { return _instance; } }

	void Awake () {
		_instance = this;
	}

	public static bool IsEnabled 
	{
		get 
		{
			if(_instance == null)
				return false;

			return _instance.IsVuforiaEnabled; 
		} 
	}

}
