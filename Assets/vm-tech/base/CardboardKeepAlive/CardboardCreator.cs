﻿using UnityEngine;
using System.Collections;

public class CardboardCreator : MonoBehaviour {
	public GameObject cardboardPrefab;
	
	// Use this for initialization
	void Awake () {
		if(FindObjectOfType<Cardboard>() == null)
			Instantiate(cardboardPrefab);
	}
}
