﻿using UnityEngine;
using System.Collections;

public class CardboardConfigurator : MonoBehaviour
{
	[SerializeField] private bool VRModeEnabled = false;
	[SerializeField] private bool EnableAlignmentMarker = false;
	[SerializeField] private bool EnableSettingsButton = false;
	[SerializeField] private Cardboard.BackButtonModes BackButtonMode;

	void Start ()
	{
		Cardboard cb = Cardboard.SDK;
		cb.VRModeEnabled = VRModeEnabled;
		cb.EnableAlignmentMarker = EnableAlignmentMarker;
		cb.EnableSettingsButton = EnableSettingsButton;

		cb.BackButtonMode = BackButtonMode;
	}
}
