﻿using UnityEngine;
using System.Collections;

public class CardboardKeepAlive : MonoBehaviour {

	void Awake () {
		DontDestroyOnLoad(gameObject);
	}

}
