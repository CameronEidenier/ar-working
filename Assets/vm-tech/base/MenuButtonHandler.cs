﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class MenuButtonHandler : MonoBehaviour, IPointerClickHandler {

	public string levelToLoad;

	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData)
	{
		Flow.ChangeScene (levelToLoad);
	}
	#endregion
}
