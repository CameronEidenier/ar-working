﻿using UnityEngine;
using System.Collections;

public class FullViewportScreen : MonoBehaviour {

	private Canvas canvas;
    private CanvasGroup canvasGroup;

	void Awake ()
    {
        canvas = GetComponent<Canvas>();
        canvasGroup = GetComponent<CanvasGroup>();
	}
	
	public void Show()
    {
        canvas.enabled = true;
	}


    public void Hide()
    {
        canvas.enabled = false;
    }
}
