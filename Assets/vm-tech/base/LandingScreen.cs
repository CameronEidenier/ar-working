﻿using UnityEngine;
using System.Collections;
using System;

public class LandingScreen : MonoBehaviour {
    private enum LandingState {
        notready,
        ready
    }

    private LandingState currentState;

    private FullViewportScreen Title;
    private FullViewportScreen PullLever;

    void Awake()
    {
        Title = transform.FindChild("InsertText").GetComponent<FullViewportScreen>();
        PullLever = transform.FindChild("LeverText").GetComponent<FullViewportScreen>();
    }

    void Start ()
    {
        Refresh();
    }

    private void Refresh()
    {
        if(currentState == LandingState.notready)
        {
            Title.Show();
            PullLever.Hide();
        }
        else if (currentState == LandingState.ready)
        {
            Title.Show();
            PullLever.Show();       
        }
    }
    
    void Update() {

        if (currentState == LandingState.ready)
        {
            if (Cardboard.SDK.Triggered)
            {
                Flow.GoToNextScreen();
            }
        }
	}
}
