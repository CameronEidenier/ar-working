﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class AnimateAround : MonoBehaviour {
    public bool exit = false;
    bool MovingToExit = false;
    public Vector3 exitPoint;
	void Start () {
        print("I'm Alive");
        CreateTweenSequence();
        //MoveToPoint1();
        /*Sequence mySeq = DOTween.Sequence();
        mySeq.Append(transform.DOMove(new Vector3(50, 0, 0),5f));
        mySeq.Append(transform.DOMove(new Vector3(0 , 0, 50), 5f));
        mySeq.Append(transform.DOMove(new Vector3(-50, 0, 0), 5f));
        mySeq.Append(transform.DOMove(new Vector3(0, 0, -50), 5f));
        mySeq.Play();*/
    }
	
	// Update is called once per frame
	void Update () {
        if (exit) ObjectExit();
        //else if (transform.position.x == 100f && transform.position.z == 100f) MoveToPoint2();
        //else if (transform.position.x == 100f && transform.position.z == -100f) MoveToPoint3();
        //else if (transform.position.x == -100f && transform.position.z == -100f) MoveToPoint4();
        //else if (transform.position.x == -100f && transform.position.z == 100f) MoveToPoint1();

        
    }
    void MoveToPoint1()
    {
        print("Run Point 1");
        transform.DOMove(new Vector3(100f,Random.Range(-010f,100f), 100f), 1f,true).SetEase(Ease.InOutCubic);
    }
    void MoveToPoint2()
    {
        print("Run Point 2");
        transform.DOMove(new Vector3(100f, Random.Range(-100f, 100f), -100f), 1f, true).SetEase(Ease.InOutCubic);
    }
    void MoveToPoint3()
    {
        print("Run Point 3");
        transform.DOMove(new Vector3(-100f, Random.Range(-100f, 100f), -100f), 1f, true).SetEase(Ease.InOutCubic);
    }
    void MoveToPoint4()
    {
        print("Run Point 4");
        transform.DOMove(new Vector3(-100f, Random.Range(-100f, 100f), 100f), 1f, true).SetEase(Ease.InOutCubic);
    }
    void ObjectExit()
    {
        if (MovingToExit == false)
        {
            //DOTween.KillAll(true);
            //DOTween.KillAll(false);
            transform.DOMove(exitPoint, 2f, true).SetEase(Ease.InOutCubic);
            MovingToExit = true;

            if (!IsInvoking("destroySelf"))
            {
                Invoke("destroySelf", 2f);
            }
        }
    }
    void destroySelf()
    {
        Destroy(gameObject);
    }
    void CreateTweenSequence()
    {
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(transform.DOMove(new Vector3(100f, Random.Range(-0100f, 100f), 100f), 2f, true).SetEase(Ease.InOutCubic));
        mySequence.Append(transform.DOMove(new Vector3(150f, Random.Range(-0100f, 100f), 0f), 2f, true).SetEase(Ease.InOutCubic));//
        mySequence.Append(transform.DOMove(new Vector3(100f, Random.Range(-100f, 100f), -100f), 2f, true).SetEase(Ease.InOutCubic));
        mySequence.Append(transform.DOMove(new Vector3(0f, Random.Range(-100f, 100f), -150f), 2f, true).SetEase(Ease.InOutCubic));//
        mySequence.Append(transform.DOMove(new Vector3(-100f, Random.Range(-100f, 100f), -100f), 2f, true).SetEase(Ease.InOutCubic));
        mySequence.Append(transform.DOMove(new Vector3(-150f, Random.Range(-100f, 100f), 0f), 2f, true).SetEase(Ease.InOutCubic));//
        mySequence.Append(transform.DOMove(new Vector3(-100f, Random.Range(-100f, 100f), 100f), 2f, true).SetEase(Ease.InOutCubic));
        mySequence.Append(transform.DOMove(new Vector3(0f, Random.Range(-100f, 100f), 150f), 2f, true).SetEase(Ease.InOutCubic));//
        mySequence.OnComplete(CreateTweenSequence);

    }
}
