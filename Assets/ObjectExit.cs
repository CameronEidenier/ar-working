﻿using UnityEngine;
using System.Collections;

public class ObjectExit : MonoBehaviour {
    GameObject[] movingObjects;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        movingObjects = GameObject.FindGameObjectsWithTag("MovingObject");
    }
    public void RecallObject()
    {
        if (movingObjects.Length > 0)
        {
            print("Exit Call");
            if (movingObjects[0].gameObject.GetComponent<AnimateAround>().exit == false)
            {
                movingObjects[0].gameObject.GetComponent<AnimateAround>().exitPoint = transform.position;
            }
            movingObjects[0].gameObject.GetComponent<AnimateAround>().exit = true;
        }
    }
}
